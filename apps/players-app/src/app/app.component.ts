import { Component } from '@angular/core';

@Component({
  selector: 'nx-work-fe-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'players-app';
}
