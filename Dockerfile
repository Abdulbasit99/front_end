FROM nginx:latest
COPY dist/angular-app/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf